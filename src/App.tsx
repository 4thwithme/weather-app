import React from "react";
import { BrowserRouter as Router } from "react-router-dom";

import Header from "./containers/Header/Header";
import ControllCenter from "./containers/ControllCenter/ControllCenter";
import InfoBlock from "./components/InfoBlock/InfoBlock";
import AdvancedInfoBlock from "./containers/AdvancedInfoBlock/AdvancedInfoBlock";

function App() {
  return (
    <Router>
      <ControllCenter>
        <>
          <Header />
          <InfoBlock />
          <AdvancedInfoBlock />
        </>
      </ControllCenter>
    </Router>
  );
}

export default App;
