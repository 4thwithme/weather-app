import secretKey from "../secret/weatherKey";
import geoKey from "../secret/geoKey";

import { AnyObject } from "../types/types";

const getWeatherByCoords = async (lat: number, lon: number) => {
  const res = await getWeatherWithParams({ lat, lon });
  return await res.json();
};

const getWeatherByCityId = async (id: number) => {
  const res = await getWeatherWithParams({ id });
  return await res.json();
};

const getWeatherWithParams = (params: AnyObject) => {
  const arrOfStrings: string[] = [];

  for (const key in params) {
    if (params.hasOwnProperty(key)) {
      arrOfStrings.push(`${key}=${params[key]}`);
    }
  }

  return fetch(
    "https://api.openweathermap.org/data/2.5/forecast/?" +
      arrOfStrings.join("&") +
      "&appid=" +
      secretKey
  );
};

const getCitiesByQuery = async (query: string) => {
  const res = await fetch(
    `https://eu1.locationiq.com/v1/search.php?key=${geoKey}&city=${query}&format=json`
  );
  return await res.json();
};

export default {
  getWeatherByCoords,
  getWeatherByCityId,
  getCitiesByQuery
};
