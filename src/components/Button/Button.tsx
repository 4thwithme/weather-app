import React, { memo } from "react";
import { IProps } from "./Button.types";

const Button = (props: IProps) => {
  return (
    <button className={props.className} onClick={() => props.onClickAction(props.type)}>
      {props.text}
    </button>
  );
};

export default memo(Button);
