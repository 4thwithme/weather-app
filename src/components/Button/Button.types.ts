import { MouseEvent } from "react";

export interface IProps {
  onClickAction: (weatherType: string) => void;
  text: string;
  className?: string;
  type: string;
}
