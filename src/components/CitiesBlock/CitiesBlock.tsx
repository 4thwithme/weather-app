import React, { useState } from "react";
import { Link } from "react-router-dom";

import { useDidMount } from "../../hooks";
import "./CitiesBlock.scss";

import Spinner from "../Spinner/Spinner";

import { IProps } from "./CitiesBlock.types";
import { connect } from "react-redux";
import { setWeatherType, getWeatherByCoords } from "../../redux/ducks/cityWeather";
import { ICityLS } from "../../containers/AdvancedInfoBlock/AdvancedInfoBlock.types";

const CitiesBlock = (props: IProps) => {
  const [savedCities, setSavedCities] = useState([]);
  const [pending, setPending] = useState(true);

  useDidMount(() => {
    const savedCitiesFromLS = JSON.parse(localStorage.getItem("savedCities") || "[]");

    setSavedCities(savedCitiesFromLS);
    setPending(false);
  });

  const handleOnChangeRoute = (city: ICityLS) => {
    props.setWeatherType("week");
    props.getWeatherByCoords(city.lat, city.lon);
  };

  if (pending) {
    return (
      <ul className='cities-list'>
        <Spinner />
      </ul>
    );
  }

  return (
    <ul className='cities-list'>
      {savedCities.length
        ? savedCities.map((city: ICityLS) => {
            return (
              <li key={city.name + city.lon} className='cities-list__item'>
                <Link to={city.name} onClick={() => handleOnChangeRoute(city)}>
                  {city.name}
                </Link>
              </li>
            );
          })
        : "Any added yet"}
    </ul>
  );
};

const mapDispatchToProps = {
  setWeatherType,
  getWeatherByCoords
};

export default connect(null, mapDispatchToProps)(CitiesBlock);
