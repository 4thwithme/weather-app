export interface IProps {
  setWeatherType: (str: string) => void;
  getWeatherByCoords: (lat: number, lon: number) => void;
}
