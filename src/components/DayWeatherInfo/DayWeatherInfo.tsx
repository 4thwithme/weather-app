import React from "react";
import { connect } from "react-redux";

import "./DayWeatherInfo.scss";
import { getDayWeatherInfo, selectCityInfo } from "../../redux/selectors";

import GoogleMapContainer from "../../containers/GoogleMapContainer/GoogleMapContainer";
import Spinner from "../Spinner/Spinner";

import { IProps, IOwnProps } from "./DayWeatherInfo.types";
import { AppState } from "../../redux/redux.types";
import { IWeatherListItem } from "../../types/types";

const DayWeatherInfo = ({ className, weatherInfo, cityInfo, ...props }: IProps) => {
  console.log(props);
  return (
    <>
      {weatherInfo.length ? (
        <>
          <div className={className}>
            <h3 className={className + "__heading"}>{props.weatherType}</h3>

            <span className={className + "__day"}>
              {cityInfo.name}, {cityInfo.country}
            </span>

            <div className={className + "__line"}>
              <span className={className + "__first-column"}>Time</span>
              <span className={className + "__second-column"}>Weather</span>
            </div>

            {weatherInfo.map((dayInfo: IWeatherListItem) => {
              return (
                <div className={className + "__line"} key={dayInfo.dt}>
                  <span className={className + "__first-column"}>
                    {dayInfo.dt_txt.slice(11, 16)}
                  </span>

                  <span className={className + "__second-column"}>
                    {(dayInfo.main.temp - 273.15).toFixed(2)} &deg;C {dayInfo.weather[0].main}, Wind
                    - {dayInfo.wind.speed} m/s
                    <b
                      className='info__wind-deg'
                      style={{ transform: `rotateZ(${dayInfo.wind.deg}deg)` }}
                    >
                      &uarr;
                    </b>
                  </span>
                </div>
              );
            })}
          </div>

          <GoogleMapContainer cityInfo={cityInfo} currentWeather={props.currentWeather} />
        </>
      ) : (
        <div className='spinner__wrp'>
          <Spinner />
        </div>
      )}
    </>
  );
};

const mapStateToProps = (state: AppState, ownProps: IOwnProps) => ({
  cityInfo: selectCityInfo(state),
  currentWeather: state.cityWeather.current,
  weatherInfo: getDayWeatherInfo(state, ownProps.weatherType)
});

export default connect(mapStateToProps)(DayWeatherInfo);
