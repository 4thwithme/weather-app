import { IWeatherListItem, ICity } from "../../types/types";

export interface IProps extends IOwnProps {
  weatherInfo: IWeatherListItem[];
  cityInfo: ICity;
  currentWeather: IWeatherListItem[];
}

export interface IOwnProps {
  className: string;
  weatherType: string;
  isWeekMode?: boolean;
}
