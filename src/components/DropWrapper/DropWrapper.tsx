import React, { useEffect, useRef } from "react";

import { IProps } from "./DropWrapper.types";

const DropWrapper = ({ dropWrapperRef, closeDropWrapper, className, style, ...props }: IProps) => {
  const selfRef: any = useRef();

  useEffect(() => {
    const ref = dropWrapperRef && dropWrapperRef.current ? dropWrapperRef : selfRef;

    const closeModalOnOutsideClick = (e: any) => {
      if (!ref.current.contains(e.target) || e.key === "Escape") {
        closeDropWrapper(false);
      }
    };

    document.addEventListener("mousedown", closeModalOnOutsideClick);
    document.addEventListener("keyup", closeModalOnOutsideClick);

    return () => {
      document.removeEventListener("mousedown", closeModalOnOutsideClick);
      document.removeEventListener("keyup", closeModalOnOutsideClick);
    };
  }, []);

  return (
    <div ref={selfRef} className={className} style={style}>
      {props.children}
    </div>
  );
};

export default DropWrapper;
