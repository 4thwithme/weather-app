import { ReactType } from "react";

import { AnyObject } from "../../types/types";

export interface IProps {
  className: string;
  style?: AnyObject;
  closeDropWrapper: (isClose: boolean) => void;
  dropWrapperRef?: any;
  children: JSX.Element;
}
