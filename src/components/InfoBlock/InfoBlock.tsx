import React from "react";
import { connect } from "react-redux";

import "./InfoBlock.scss";
import { selectCurrentWeather, selectCityInfo } from "../../redux/selectors";

import Spinner from "../Spinner/Spinner";

import { AppState } from "../../redux/redux.types";
import { IProps } from "./InfoBlock.types";
import Button from "../Button/Button";
import { ICity } from "../../types/types";

const InfoBlock = ({ cityInfo, ...props }: IProps): JSX.Element => {
  const weatherInfo = props.weatherInfo[0];

  const saveCity = (city: ICity) => {
    const savedCities: any = JSON.parse(localStorage.getItem("savedCities") || "[]");

    const updatedSavedCities = savedCities.some((savedCity: ICity) => savedCity.id === city.id)
      ? savedCities
      : [...savedCities, { lat: city.coord.lat, lon: city.coord.lon, name: city.name }];

    localStorage.setItem("savedCities", JSON.stringify(updatedSavedCities));
  };

  if (!weatherInfo || !cityInfo) {
    return (
      <div className='info'>
        <Spinner />
      </div>
    );
  }

  return (
    <div className='info'>
      <Button
        text='&#43;'
        type='button'
        className='save-btn'
        onClickAction={() => saveCity(cityInfo)}
      />

      <span className='info__today'>Today</span>

      <span className='info__temperature'>
        {(weatherInfo.main.temp - 273.15).toFixed(2)} &deg;C
      </span>

      <span className='info__city'>
        {cityInfo.name}, {cityInfo.country}
      </span>

      <span className='info__weather'>
        {weatherInfo.weather[0].main}, Wind - {weatherInfo.wind.speed} meters per second
        <b className='info__wind-deg' style={{ transform: `rotateZ(${weatherInfo.wind.deg}deg)` }}>
          &uarr;
        </b>
      </span>
    </div>
  );
};

const mapStateToProps = (state: AppState) => ({
  weatherInfo: selectCurrentWeather(state),
  cityInfo: selectCityInfo(state)
});

export default connect(mapStateToProps)(InfoBlock);
