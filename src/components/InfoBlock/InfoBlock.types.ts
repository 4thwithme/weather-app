import { IWeatherListItem, ICity } from "../../types/types";

export interface IProps {
  weatherInfo: IWeatherListItem[];
  cityInfo: ICity;
}
