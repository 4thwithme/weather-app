import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";

import { useDebounce, useDidUpdate, useDidMount } from "../../hooks";

import "./Searcher.scss";

type SearchOption = string | number | null;
interface ISearchInputProps {
  query: string;
  setQuery: (query: string) => void;
  stopSearch: () => void;
  startSearch: (query: string, searchOption?: SearchOption) => void;
  searchOption?: SearchOption;
  classPrefix?: string;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  inputRef?: React.RefObject<HTMLInputElement>;
  isAutoFocus?: boolean;
  isDisabled?: boolean;
  color?: string;
}

const SearchInput = (props: ISearchInputProps): JSX.Element => {
  const {
    searchOption,
    query,
    classPrefix,
    onChange,
    color,
    setQuery,
    stopSearch,
    startSearch,
    inputRef,
    isDisabled,
    isAutoFocus,
    ...inputProps
  } = props;

  useDidMount(() => {
    inputRef && inputRef.current && props.isAutoFocus && inputRef.current.focus();
  });

  useDidUpdate(() => {
    if (query.length === 0) {
      stopSearch();
    }
  }, [query]);

  const handleSearch = (): void => {
    if (query.length >= 2) {
      startSearch(query, searchOption); // searchOption means some additional param to search function
    }
  };

  const onInputChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    if (onChange) {
      onChange(e);
    }
    setQuery(e.target.value);
  };

  useDebounce(handleSearch, 300, query);

  return (
    <div className={"search-input " + classPrefix + "__input-wrap"}>
      <input
        autoComplete='off'
        className={"search-input__input " + classPrefix + "__search-input"}
        maxLength={20}
        placeholder='Search'
        tabIndex={0}
        type='search'
        value={query}
        ref={inputRef}
        onChange={onInputChange}
        {...inputProps}
      />

      <FontAwesomeIcon
        icon={faSearch}
        color={color || "#000"}
        className={"search-input__icon " + classPrefix + "__search-input-icon"}
      />
    </div>
  );
};

export default React.memo(SearchInput);
