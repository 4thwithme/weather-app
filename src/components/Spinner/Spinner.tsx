import React from "react";

interface ISpinnerProps {
  className?: string;
  size?: string;
}

const Spinner = (props: ISpinnerProps): JSX.Element => {
  const { size = "30px", className = "" } = props;

  return (
    <img
      src='/media/spinner.gif'
      style={{ width: size, height: size }}
      className={className}
      alt='Loading...'
    />
  );
};

export default Spinner;
