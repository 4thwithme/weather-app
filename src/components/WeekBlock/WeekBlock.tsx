import React from "react";
import { connect } from "react-redux";

import { convertDayToString } from "../../utils";
import { setWeatherType, getWeatherByCoords } from "../../redux/ducks/cityWeather";
import "./WeekBlock.scss";

import Spinner from "../Spinner/Spinner";

import { IProps } from "./WeekBlock.type";
import { AppState } from "../../redux/redux.types";
import { useDidMount } from "../../hooks";

const WeekBlock = (props: IProps) => {
  useDidMount(() => {
    props.city && props.getWeatherByCoords(props.city.lat, props.city.lon);
  });

  const getDay = (dateString: string) => {
    const date = new Date(dateString);

    return date.getDay();
  };

  const openDayWeatherBlock = (weatherType: string) => {
    props.setWeatherType(weatherType);
  };

  if (!props.week[0].length) {
    return (
      <ul className='week-list'>
        <Spinner />
      </ul>
    );
  }

  return (
    <ul className='week-list'>
      {props.week.map((dayWeather) => {
        const day = convertDayToString(getDay(dayWeather[0].dt_txt.slice(0, 10)));

        return (
          <li
            key={dayWeather[0].dt}
            className='week-list-item'
            onClick={() => openDayWeatherBlock(day)}
          >
            {day}
          </li>
        );
      })}
    </ul>
  );
};

const mapStateToProps = (state: AppState) => ({
  week: state.cityWeather.week
});

const mepDispatchToProps = {
  setWeatherType,
  getWeatherByCoords
};

export default connect(mapStateToProps, mepDispatchToProps)(WeekBlock);
