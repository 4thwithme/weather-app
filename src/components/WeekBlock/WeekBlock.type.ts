import { IWeatherListItem } from "../../types/types";
import { ICityLS } from "../../containers/AdvancedInfoBlock/AdvancedInfoBlock.types";

export interface IProps {
  week: IWeatherListItem[][];
  setWeatherType: (type: string) => void;
  city?: ICityLS;
  getWeatherByCoords: (lat: number, lon: number) => void;
}
