import React, { useState } from "react";
import { Switch, Route } from "react-router-dom";
import { connect } from "react-redux";

import "./AdvancedInfoBlock.scss";

import DayWeatherInfo from "../../components/DayWeatherInfo/DayWeatherInfo";

import { AppState } from "../../redux/redux.types";
import { IProps, ICityLS } from "./AdvancedInfoBlock.types";
import WeekBlock from "../../components/WeekBlock/WeekBlock";
import CitiesBlock from "../../components/CitiesBlock/CitiesBlock";
import { useDidMount } from "../../hooks";

const AdvancedInfoBlock = ({ weatherType }: IProps) => {
  const [citiesList, setCitiesList] = useState([]);

  useDidMount(() => {
    const citiesLS = JSON.parse(localStorage.getItem("savedCities") || "[]");

    citiesLS.length && setCitiesList(citiesLS);
  });

  const render = (city?: ICityLS) => {
    switch (weatherType) {
      case "today":
      case "tomorrow":
        return <DayWeatherInfo className='day-info' weatherType={weatherType} />;
      case "week":
        return <WeekBlock />;
      case "cities":
        return <CitiesBlock />;
      default:
        return <DayWeatherInfo className='day-info' isWeekMode weatherType={weatherType} />;
    }
  };

  return (
    <Switch>
      <Route exact path='/'>
        <div className='adv-info'>{render()}</div>
      </Route>

      {citiesList.map((city: ICityLS, i: number) => {
        return (
          <Route exact path={`/${city.name}`} key={city.name + i}>
            <div className='adv-info'>
              {/* <WeekBlock city={city} /> */}
              {render()}
            </div>
          </Route>
        );
      })}
    </Switch>
  );
};

const mapStateToProps = (state: AppState) => ({
  weatherType: state.cityWeather.weatherType
});

export default connect(mapStateToProps)(AdvancedInfoBlock);
