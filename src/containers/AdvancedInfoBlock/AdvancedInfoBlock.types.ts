export interface IProps {
  weatherType: string;
}

export interface ICityLS {
  lat: number;
  lon: number;
  name: string;
}
