import React from "react";
import { connect } from "react-redux";

import "./ControllCenter.scss";
import { getCurrentCoords } from "../../utils";
import { getWeatherByCoords } from "../../redux/ducks/cityWeather";
import useDidMount from "../../hooks/useDidMount";

import { IProps } from "./ControllCenter.types";

const ControllCenter = (props: IProps): JSX.Element => {
  useDidMount(() => {
    getCurrentCoords(props.getWeatherByCoords);
  });

  return <div className='layout'>{props.children}</div>;
};

const mapDispatchToProps = {
  getWeatherByCoords
};

export default connect(null, mapDispatchToProps)(ControllCenter);
