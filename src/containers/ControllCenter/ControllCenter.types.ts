import { ReactType } from "react";

export interface IProps {
  children: JSX.Element;
  getWeatherByCoords: (lat: number, lon: number) => void;
}
