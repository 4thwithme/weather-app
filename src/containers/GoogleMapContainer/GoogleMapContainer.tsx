import React, { useState, Fragment, useEffect } from "react";
import ReactDOM from "react-dom";
import { Marker, GoogleApiWrapper, Map } from "google-maps-react";

import googleMapKey from "../../secret/googleMapsKey";
import DropWrapper from "../../components/DropWrapper/DropWrapper";
import Spinner from "../../components/Spinner/Spinner";
import { ICoord } from "../../types/types";

const GoogleMapContainer = ({ cityInfo, currentWeather, ...props }: any) => {
  const [modal, setModal] = useState({
    isOpen: false,
    coord: {
      x: 0,
      y: 0
    }
  });

  const [currentCoords, setCurrentCoords] = useState<ICoord>({
    lat: cityInfo.coord.lat || 0,
    lon: cityInfo.coord.lon || 0
  });

  useEffect(() => {
    setCurrentCoords({
      lat: cityInfo.coord.lat || 0,
      lon: cityInfo.coord.lon || 0
    });
  }, [cityInfo]);

  const weatherInfo = currentWeather[0];

  const handleOnClick = (props: any, marker: any, e: any) => {
    setModal({ coord: { x: e.tb.pageX, y: e.tb.pageY }, isOpen: true });
  };

  return (
    <>
      <Map
        initialCenter={{
          lat: currentCoords.lat,
          lng: currentCoords.lon
        }}
        center={{
          lat: currentCoords.lat,
          lng: currentCoords.lon
        }}
        google={props.google}
        zoom={12}
      >
        <Marker
          onClick={handleOnClick}
          position={{
            lat: currentCoords.lat,
            lng: currentCoords.lon
          }}
        />
      </Map>

      {modal.isOpen &&
        ReactDOM.createPortal(
          <DropWrapper
            className='modal'
            style={{
              left: modal.coord.x,
              top: modal.coord.y
            }}
            closeDropWrapper={() => setModal({ ...modal, isOpen: false })}
          >
            <Fragment>
              <span>Weather now</span>
              <span className='modal__temperature'>
                {(weatherInfo.main.temp - 273.15).toFixed(2)} &deg;C
              </span>

              <span className='modal__city'>
                {cityInfo.name}, {cityInfo.country}
              </span>

              <span className='modal__weather'>
                {weatherInfo.weather[0].main}, Wind - {weatherInfo.wind.speed} m/s
                <b
                  className='modal__wind-deg'
                  style={{ transform: `rotateZ(${weatherInfo.wind.deg}deg)` }}
                >
                  &uarr;
                </b>
              </span>
            </Fragment>
          </DropWrapper>,
          document.getElementById("modal") || document.createElement("div")
        )}
    </>
  );
};

export default GoogleApiWrapper({
  apiKey: googleMapKey,
  LoadingContainer: Spinner
})(GoogleMapContainer);
