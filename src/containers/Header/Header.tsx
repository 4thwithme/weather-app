import React, { useState, useCallback, useRef } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import "./Header.scss";
import { setWeatherType, getWeatherByCoords } from "../../redux/ducks/cityWeather";
import getCurrentPosition from "../../utils/getCurrentCoords";

import Button from "../../components/Button/Button";
import Searcher from "../../components/Searcher/Searcher";

import { IProps, ISearchedCity } from "./Header.types";
import API from "../../api/api";
import DropWrapper from "../../components/DropWrapper/DropWrapper";

const Header = (props: IProps): JSX.Element => {
  const [query, setQuery] = useState("");
  const [results, setResult] = useState([]);

  const inputRef: any = useRef();

  const memorizedStartSearch = useCallback((query) => {
    API.getCitiesByQuery(query)
      .then((res) => {
        setResult(res);
      })
      .catch(console.error);
  }, []);

  const memorizedStopSearch = useCallback(() => {
    setQuery("");
    setResult([]);
  }, []);

  const handleOnClose = () => {
    setQuery("");
    setResult([]);
  };

  const setNewCityWeather = (result: ISearchedCity) => {
    handleOnClose();
    props.getWeatherByCoords(+result.lat, +result.lon);
  };

  const redirectToHome = (type: string) => {
    props.setWeatherType(type);
    getCurrentPosition(props.getWeatherByCoords);
  };

  return (
    <header className='header'>
      <Button
        type='today'
        text='Today'
        className='header__btn'
        onClickAction={props.setWeatherType}
      />
      <Button
        type='tomorrow'
        text='Tomorrow'
        className='header__btn'
        onClickAction={props.setWeatherType}
      />
      <Button
        type='week'
        text='Week'
        className='header__btn'
        onClickAction={props.setWeatherType}
      />
      <Button
        type='cities'
        text='Saved Cities'
        className='header__btn'
        onClickAction={props.setWeatherType}
      />

      <Link to='/'>
        <Button
          type='today'
          text='Back to Main'
          className='header__btn'
          onClickAction={redirectToHome}
        />
      </Link>

      <div ref={inputRef} className='header__searcher-wrap'>
        <Searcher
          query={query}
          setQuery={setQuery}
          stopSearch={memorizedStopSearch}
          startSearch={memorizedStartSearch}
          classPrefix='header'
          // onChange={}
          isAutoFocus
          color='#888'
          isDisabled={false}
        />

        {results.length ? (
          <DropWrapper closeDropWrapper={handleOnClose} className='' dropWrapperRef={inputRef}>
            <ul className='header__searcher-list'>
              {results.map((result: ISearchedCity) => (
                <li
                  className='header__searcher-list-item'
                  key={result.place_id}
                  onClick={() => setNewCityWeather(result)}
                >
                  {result.display_name.split(" ")[0]}
                </li>
              ))}
            </ul>
          </DropWrapper>
        ) : null}
      </div>
    </header>
  );
};

const mapDispatchToProps = {
  setWeatherType,
  getWeatherByCoords
};

export default connect(null, mapDispatchToProps)(Header);
