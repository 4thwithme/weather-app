export interface IProps {
  setWeatherType: (weatherType: string) => void;
  getWeatherByCoords: (lat: number, lon: number) => void;
}

export interface ISearchedCity {
  place_id: string;
  licence: string;
  osm_type: string;
  osm_id: string;
  boundingbox: [];
  lat: string;
  lon: string;
  display_name: string;
  class: string;
  type: string;
  importance: number;
  icon: string;
}
