import { Dispatch } from "redux";

import API from "../../api/api";

import { IAction, ICityWeatherInitialState } from "../redux.types";
import { IWeatherListItem } from "../../types/types";
import { convertDayToString } from "../../utils";

export const GET_WEEKLY_WEATHER_BY_CITY_ID = "GET_WEEKLY_WEATHER_BY_CITY_ID";
export const SET_WETHAER_TYPE = "SET_WETHAER_TYPE";

export const getWeatherByCityId = (cityId: number) => (dispatch: Dispatch) => {
  API.getWeatherByCityId(cityId)
    .then((data) => dispatch({ type: GET_WEEKLY_WEATHER_BY_CITY_ID, payload: data }))
    .catch(console.error);
};

export const getWeatherByCoords = (lat: number, lon: number) => (dispatch: Dispatch) => {
  API.getWeatherByCoords(lat, lon)
    .then((data) => dispatch({ type: GET_WEEKLY_WEATHER_BY_CITY_ID, payload: data }))
    .catch(console.error);
};

export const setWeatherType = (weatherType: string) => (dispatch: Dispatch) => {
  dispatch({
    type: SET_WETHAER_TYPE,
    payload: weatherType
  });
};

const initialState: ICityWeatherInitialState = {
  current: [],
  today: [],
  tomorrow: [],
  week: [[]],
  city: {},
  weatherType: "today",
  weekDaysMap: {}
};

export default (state = initialState, { type, payload }: IAction) => {
  switch (type) {
    case GET_WEEKLY_WEATHER_BY_CITY_ID: {
      const todayList: IWeatherListItem[] = [];
      const tomorrowList: IWeatherListItem[] = [];

      const date = new Date();
      const today = date.getDate();

      const nextDate = new Date(date.getFullYear(), date.getMonth(), today + 1);
      const tomorrow = nextDate.getDate();

      payload.list.forEach((listItem: IWeatherListItem) => {
        const itemDate = new Date(listItem.dt_txt.slice(0, 10));

        if (today === itemDate.getDate()) {
          todayList.push(listItem);
        }
        if (tomorrow === itemDate.getDate()) {
          tomorrowList.push(listItem);
        }
      });

      const { sortedArr, weekDaysMap } = sortByDay(payload.list);

      return {
        ...state,
        current: [payload.list[0]],
        today: todayList,
        tomorrow: tomorrowList,
        week: sortedArr,
        city: payload.city,
        weekDaysMap
      };
    }

    case SET_WETHAER_TYPE: {
      return {
        ...state,
        weatherType: payload
      };
    }

    default:
      return state;
  }
};

//redux helpers-----
const sortByDay = (list: IWeatherListItem[]) => {
  let currentDate = new Date(list[0].dt_txt.slice(0, 10));
  let currentDay = currentDate.getDate();

  const weekDaysMap = {
    [convertDayToString(currentDate.getDay())]: 0
  };

  const sortedArr: IWeatherListItem[][] = [[]];

  list.forEach((item) => {
    const itemDate = new Date(item.dt_txt.slice(0, 10));

    if (itemDate.getDate() === currentDay) {
      sortedArr[sortedArr.length - 1].push(item);
    } else {
      const newDate = new Date(item.dt_txt.slice(0, 10));
      currentDay = newDate.getDate();

      weekDaysMap[convertDayToString(newDate.getDay())] = sortedArr.length;

      sortedArr.push([item]);
    }
  });

  return { sortedArr, weekDaysMap };
};

//redux helpers-----
