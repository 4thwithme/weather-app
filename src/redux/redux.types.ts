import rootReducer from "./rootReducer";

import { IWeatherListItem, ICity } from "../types/types";

export type AppState = ReturnType<typeof rootReducer>;

export interface IAction {
  type: string;
  payload: any;
}

export interface ICityWeatherInitialState {
  current: IWeatherListItem[] | [];
  today: IWeatherListItem[] | [];
  tomorrow: IWeatherListItem[] | [];
  week: IWeatherListItem[][] | [][];
  city: ICity | {};
  weatherType: string;
  weekDaysMap: { [key: string]: number };
}
