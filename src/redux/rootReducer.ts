import { combineReducers } from "redux";

import cityWeather from "./ducks/cityWeather";

const rootReducer = combineReducers({
  cityWeather
});

export default rootReducer;
