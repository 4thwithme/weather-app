import { createSelector } from "reselect";

import { AppState } from "./redux.types";
import { weekDaysArr } from "../constants";

export const selectCurrentWeather = createSelector(
  (state: AppState) => state.cityWeather.current,

  (current) => current
);

export const selectCityInfo = createSelector(
  (state: AppState) => state.cityWeather.city,

  (city) => city
);

export const getDayWeatherInfo = createSelector(
  (state: AppState, weatherType: string) => state.cityWeather,
  (state: AppState, weatherType: string) => weatherType,

  (cityWeather, weatherType) => {
    if ("today" === weatherType || "tomorrow" === weatherType) {
      return cityWeather[weatherType];
    } else if (weekDaysArr.includes(weatherType)) {
      const weekDayNumber = cityWeather.weekDaysMap[weatherType];
      return cityWeather.week[weekDayNumber] || [];
    }
    return [];
  }
);
