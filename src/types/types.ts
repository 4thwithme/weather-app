export interface ICoord {
  lat: number;
  lon: number;
}

export interface IWeatherListItem {
  dt: number;
  main: AnyObject;
  weather: AnyObject[];
  clouds: AnyObject;
  wind: AnyObject;
  rain: AnyObject;
  sys: AnyObject;
  dt_txt: string;
}

export interface ICity {
  id: number;
  name: string;
  coord: ICoord;
  country: string;
  timezone: number;
  sunrise: number;
  sunset: number;
}

export interface AnyObject {
  [key: string]: any;
}

export interface IWeatherType {
  weatherType: string;
}
