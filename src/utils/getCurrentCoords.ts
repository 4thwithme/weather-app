export default (fn: any) => {
  if (!window.navigator.geolocation) {
    fn && fn(0, 0);
  } else {
    window.navigator.geolocation.getCurrentPosition((pos: Position): void => {
      fn && fn(pos.coords.latitude, pos.coords.longitude);
    }, console.error);
  }
};
