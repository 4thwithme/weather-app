import getCurrentCoords from "./getCurrentCoords";
import convertDayToString from "./convertDayToString";

export { getCurrentCoords, convertDayToString };
